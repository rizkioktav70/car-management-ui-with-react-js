import React from 'react';
import MainCars from "../components/mainCars"
import Header from "../components/header"
import Footer from "../components/footer"

const index = () => {
    return (
        <div>
            <Header />
            <MainCars />
            <Footer />
        </div>
    )
}

export default index