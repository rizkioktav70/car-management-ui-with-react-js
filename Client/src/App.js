import React from 'react';
import Index from "./pages/index"
import Cars from "./pages/cars"
import { BrowserRouter, Routes, Route } from "react-router-dom";
import { Provider } from "react-redux";
import store from "./redux/store";


function App() {
  return (
    <Provider store={store}>
      <BrowserRouter>
        <Routes>
          <Route path="/" element={<Index />} />
          <Route path="/Cars" element={<Cars />} />
        </Routes>
      </BrowserRouter>
    </Provider>

  );
}

export default App;
