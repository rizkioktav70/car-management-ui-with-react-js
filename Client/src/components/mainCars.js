import React from "react";
import Jumbotron from "./content/jumbotron"
import Search from "./content/contentCars/formSearchCar"

const main = () => {
    return (
        <div>
            <div className="container-header">
                <Jumbotron btn={false} />
            </div>
            <Search />
        </div>

    )
}

export default main