import React from "react";

const getstart = () => {
    return (
        <div className="container-footer">
            <div className="row">
                <div className="col-sm-3">
                    <p>Jalan Suroyo No. 161 Mayangan Kota Probolonggo 672000</p>
                    <p>binarcarrental@gmail.com</p>
                    <p>081-233-334-808</p>
                </div>
                <div className="col-sm-2 fw-bold">
                    <ul className="list-unstyled">
                        <li className="pb-3"><a href="#service">Our services</a></li>
                        <li className="pb-3"><a href="#whyus">Why Us</a></li>
                        <li className="pb-3"><a href="#testimonial">Testimonial</a></li>
                        <li className="pb-sm-3"><a href="#faq">FAQ</a></li>
                    </ul>
                </div>
                <div className="col-sm-4">
                    <p>Connect with us</p>
                    <ul className="list-unstyled d-flex">
                        <li><a href="#"><img className="icon-sosmed" src="./assets/img/icons/icon_facebook.png" alt="" /></a></li>
                        <li><a href="#"><img className="icon-sosmed" src="./assets/img/icons/icon_instagram.png" alt="" /></a></li>
                        <li><a href="#"><img className="icon-sosmed" src="./assets/img/icons/icon_twitter.png" alt="" /></a></li>
                        <li><a href="#"><img className="icon-sosmed" src="./assets/img/icons/icon_mail.png" alt="" /></a></li>
                        <li><a href="#"><img className="icon-sosmed" src="./assets/img/icons/icon_twitch.png" alt="" /></a></li>
                    </ul>
                </div>
                <div className="col-sm-3">
                    <p>Copyright Binar 2022</p>
                    <img src="./assets/img/objects/logo.png" alt="" />
                </div>
            </div>
        </div>
    )
}

export default getstart