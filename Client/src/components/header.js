import React from "react";
import { Link } from "react-router-dom";
const header = () => {
    return (
        <div className="container-header">
            <nav className="navbar">
                <div>
                    <Link to="/">
                        <img src="./assets/img/objects/logo.png" alt="" />
                    </Link>
                </div>
                <div>
                    <ul className="nav">
                        <li className="nav-item"><a href="#service" className="nav-link" style={{ color: "black" }}>Our Services</a></li>
                        <li className="nav-item"><a href="#whyus" className="nav-link" style={{ color: "black" }}>Why Us</a></li>
                        <li className="nav-item"><a href="#testimonial" className="nav-link" style={{ color: "black" }}>Testimonial</a></li>
                        <li className="nav-item"><a href="#faq" className="nav-link" style={{ color: "black" }}>FAQ</a></li>
                        <li className="nav-item">
                            <div className="justify-content-center text-end">
                                <button type="button" className="btn btn-success">Register</button>
                            </div>
                        </li>
                    </ul>
                </div>
            </nav>
        </div>
    )
}

export default header