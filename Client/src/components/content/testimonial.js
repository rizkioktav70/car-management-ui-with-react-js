import React from "react"
import OwlCarousel from "react-owl-carousel"
import "owl.carousel/dist/assets/owl.carousel.css"
import "owl.carousel/dist/assets/owl.theme.default.css"

const carousel = () => {
    return (<div>
        <div class="container" id="testimonial">
            <h2 class="text-center pt-5">Testimonial</h2>
            <p class="text-center">Berbagai review positif dari para pelanggan kami</p>
        </div>
        <OwlCarousel className="owl-carousel owl-theme"
            loop={true}
            center={true}
            nav={true}
            navText={["<img className='button-carousel' id='left-button' src='./assets/img/icons/Left_button_hover.png'/>",
                "<img className='button-carousel' id='right-button' src='./assets/img/icons/Right_button_hover.png'/>"]}
            dots={false}
            autoplay={true}
            autoplayTimeout={5000}
            smartSpeed={2000}
            responsive={{
                600: {
                    items: 2,
                },
            }}>
            <div className="item">
                <div className="row">
                    <div className="col-3">
                        <div className="photo">
                            <img src="./assets/img/objects/photo.png" alt="" />
                        </div>
                    </div>
                    <div className="col-9">
                        <div className="rate">
                            <img src="./assets/img/icons/Rate.png" alt="" />
                        </div>
                        <div className="comment">
                            <p>“Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod lorem ipsum dolor sit amet,
                                consectetur adipiscing elit, sed do eiusmod lorem ipsum dolor sit amet, consectetur adipiscing elit, sed
                                do eiusmod”</p>
                            <p className="fw-bold">
                                Killua Kun temennya Rizki, Semarang
                            </p>
                        </div>
                    </div>
                </div>
            </div>
            <div className="item">
                <div className="row">
                    <div className="col-3">
                        <div className="photo">
                            <img src="./assets/img/objects/photo2.png" alt="" />
                        </div>
                    </div>
                    <div className="col-9">
                        <div className="rate">
                            <img src="./assets/img/icons/Rate.png" alt="" />
                        </div>
                        <div className="comment">
                            <p>“Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod lorem ipsum dolor sit amet,
                                consectetur adipiscing elit, sed do eiusmod lorem ipsum dolor sit amet, consectetur adipiscing elit, sed
                                do eiusmod”</p>
                            <p className="fw-bold">
                                John Dee 32, Bromo
                            </p>
                        </div>
                    </div>
                </div>
            </div>
            <div className="item">
                <div className="row">
                    <div className="col-3">
                        <div className="photo">
                            <img src="./assets/img/objects/photo3.png" alt="" />
                        </div>
                    </div>
                    <div className="col-9">
                        <div className="rate">
                            <img src="./assets/img/icons/Rate.png" alt="" />
                        </div>
                        <div className="comment">
                            <p>“Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod lorem ipsum dolor sit amet,
                                consectetur adipiscing elit, sed do eiusmod lorem ipsum dolor sit amet, consectetur adipiscing elit, sed
                                do eiusmod”</p>
                            <p className="fw-bold">
                                Sabrina Dee 22, Bali
                            </p>
                        </div>
                    </div>
                </div>
            </div>
        </OwlCarousel >
    </div>
    )
}

export default carousel