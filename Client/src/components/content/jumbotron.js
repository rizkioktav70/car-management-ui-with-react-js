import React from "react";
import { Link } from 'react-router-dom';

const jumbotron = ({ btn = true }) => {
    return (
        <div className="row-jumbotron">
            <div className="col-jumbotron">
                <h1 className="sm-me-5">Sewa & Rental Mobil Terbaik di kawasan Semarang</h1>
                <p className="me-3 sm-me-5">Selamat datang di Binar Car Rental. Kami menyediakan mobil kualitas
                    terbaik dengan harga terjangkau. Selalu siap melayani kebutuhanmu
                    untuk sewa mobil selama 24 jam.</p>
                {btn && (
                    <Link to="/Cars">
                        <button className="btn btn-success" type="button">
                            Mulai Sewa Mobil
                        </button>
                    </Link>
                )}

            </div>
            <div className="col">
                <img className="img-jumbotron" src="./assets/img/objects/img_car.png" alt="" />
            </div>
        </div>
    )
}

export default jumbotron