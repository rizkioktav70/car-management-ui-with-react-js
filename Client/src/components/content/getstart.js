import React from "react";
import { Link } from 'react-router-dom';

const getstart = () => {
    return (
        <div className="container-getstart">
            <h1 className="text-center text-white">Sewa Mobil di Semarang Sekarang</h1>
            <p className="text-center text-white py-2">Lorem ipsum
                dolor sit amet consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.
            </p>
            <Link to="/Cars">
                <button className="btn btn-success d-grid gap-2 mx-auto mt-5" type="button" style={{ color: "white" }}>
                    Mulai Sewa Mobil
                </button>
            </Link>
        </div >
    )
}

export default getstart