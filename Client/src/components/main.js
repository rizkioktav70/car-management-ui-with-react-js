import React from "react";
import Jumbotron from "./content/jumbotron"
import Service from "./content/service"
import WhyUs from "./content/whyus"
import GetStart from "./content/getstart"
import FAQ from "./content/faq"
import Testimonial from "./content/testimonial"
const main = () => {
    return (
        <div>
            <div className="container-header">
                <Jumbotron />
            </div>
            <Service />
            <WhyUs />
            <Testimonial />
            <GetStart />
            <FAQ />
        </div>

    )
}

export default main