const express = require("express");
const cors = require("cors");
const axios = require("axios").default;
const app = express();
const port = 1000;
const { changeDateCars, filterCars } = require("./helper");

app.use(express.json());
app.use(cors());

app.get("/", (req, res) => {
    res.send({
        message: "Success get data",
    });
});

app.post("/api/cars", async (req, res) => {
    const cars = (await axios.get("https://raw.githubusercontent.com/fnurhidayat/probable-garbanzo/main/data/cars.min.json")).data;
    const newCars = await changeDateCars(cars);
    const filteredCars = await filterCars(newCars, req.body);
    res.send(filteredCars);
});

app.listen(port, () => {
    console.log(`Running server success`);
    console.log(`You can check it at http://localhost:${port}`);
});
